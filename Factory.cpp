#include "Factory.h"

Factory::Factory() {}

Factory::~Factory() {}

void Factory::AddPieces(const std::array<Piece, 4>& pieces)
{
	if (IsEmpty())
	{
		m_factoryPieces = pieces;
	}
}

uint16_t Factory::NumberOfPickedPiece(const Piece& piece)
{
	uint16_t nrPieces = 0;

	for (int index = 0; index < 4; index++)
	{
		if (m_factoryPieces[index] == piece)
		{
			nrPieces++;
			m_factoryPieces[index] = Piece::Color::Null;
		}
	}

	return nrPieces;
}

std::vector<Piece> Factory::ReturnRestOfPieces()
{
	std::vector<Piece> temp;

	for (int index = 0; index < 4; index++)
	{
		if (m_factoryPieces[index] != Piece::Color::Null)
		{
			temp.push_back(m_factoryPieces[index]);
			m_factoryPieces[index] = Piece::Color::Null;
		}
	}

	return temp;
}

std::array<Piece, 4> Factory::GetPieces()
{
	return m_factoryPieces;
}

bool Factory::IsEmpty()
{
	for (int index = 0; index < 4; index++)
		if (m_factoryPieces[index].GetColor() != Piece::Color::Null)
			return false;
	return true;
}

bool Factory::Contains(const Piece& piece)
{
	for (uint16_t index = 0; index < 4; index++)
		if (m_factoryPieces[index] == piece)
			return true;
	return false;
}

Factory::Factory(const Factory& factory)
{
	m_factoryPieces = factory.m_factoryPieces;
}

Factory::Factory(Factory&& factory)
{
	*this = std::move(factory);
}

Factory& Factory::operator=(const Factory& factory)
{
	m_factoryPieces = factory.m_factoryPieces;
	return *this;
}

Factory& Factory::operator=(Factory&& factory)
{
	if (this != &factory)
	{
		m_factoryPieces = factory.m_factoryPieces;
	}

	return *this;
}

std::ostream& operator<<(std::ostream& os, const Factory& factory)
{
	std::for_each(factory.m_factoryPieces.begin(),factory.m_factoryPieces.end(),[](Piece e){if(e!=Piece::Color::Null)std::cout<<e;});
	return os;
}
