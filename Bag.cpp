#include <algorithm> 
#include <random>
#include <chrono>

#include "Bag.h"

Bag::Bag()
{
	for (int16_t index = 0; index < 20; index++)
	
		m_bag.push_back(Piece::Color::Black);
	
	for (int16_t index = 0; index < 20; index++)
	
		m_bag.push_back(Piece::Color::Blue);
	
	for (int16_t index = 0; index < 20; index++)
	
		m_bag.push_back(Piece::Color::Red);

	for (int16_t index = 0; index < 20; index++)
	
		m_bag.push_back(Piece::Color::White);

	for (int16_t index = 0; index < 20; index++)
	
		m_bag.push_back(Piece::Color::Yellow);
	

	uint16_t seed = std::chrono::system_clock::now().time_since_epoch().count();
	shuffle(m_bag.begin(), m_bag.end(), std::default_random_engine(seed));
}

Bag::~Bag() {}

std::vector<Piece> Bag::GetBag()
{
	return m_bag;
}

Piece Bag::GetOnePiece()
{
	//get random int, if piece at that position is null, random again until I find it, after I find it, return and remove it from the bag
	Piece temp;
	srand(time(NULL));

	uint16_t randomPiecesIndex = rand() % 100;

	while (m_bag.at(randomPiecesIndex) != Piece::Color::Null)
	{
		uint16_t randomPiecesIndex = rand() % 100;
	}

	temp = m_bag.at(randomPiecesIndex);
	m_bag.erase(m_bag.begin() + randomPiecesIndex);

	return temp;
}

std::array<Piece, 4> Bag::GetFourPieces()
{
	//same as before, keep getting random piece until it is not null so I can add it to randomPieces array, after I ve found 4 pieces and replace them with null, return the array
	if(!IsEmpty()){
	std::array<Piece, 4> pickedPieces;
	srand(time(NULL));

	uint16_t index = 0;
	uint16_t randomPieceIndex = 0;

	while (!IsEmpty() && index < 4)
	{
		randomPieceIndex = rand() % 100;

		if (randomPieceIndex < m_bag.size())
		{
			if(m_bag.at(randomPieceIndex)!=Piece::Color::Null)
			{
			pickedPieces[index] = m_bag.at(randomPieceIndex);
			m_bag.erase(m_bag.begin() + randomPieceIndex);
			index++;
			}
		}
	}
	return pickedPieces;
	}
	else
	std::cout<<"\nBag is empty!";

	
}

bool Bag::IsEmpty()
{
	return m_bag.empty();
}

void Bag::AddPieces(const std::vector<Piece>& pieces,Logger& logger)
{
	for (Piece index : pieces)
	{
		m_bag.push_back(index);
		std::cout<<"\n"<<index<<" added back to bag";
		logger.Log("Piece added back to bag",Logger::Level::Info);
	}
}

Bag::Bag(const Bag& bag)
{
	m_bag = bag.m_bag;
}

Bag::Bag(Bag&& bag)
{
	*this = std::move(bag);
}

Bag& Bag::operator=(const Bag& bag)
{
	m_bag = bag.m_bag;
	return *this;
}

Bag& Bag::operator=(Bag&& bag)
{
	if (this != &bag)
	{
		m_bag = bag.m_bag;
	}
	return *this;
}

std::ostream& operator<<(std::ostream& os, const Bag& bag)
{
	for (Piece index : bag.m_bag)
		std::cout << index;
	return os;
}
