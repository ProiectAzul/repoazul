#include "AzulGame.h"

AzulGame::AzulGame()
{
}

AzulGame::~AzulGame()
{
}


bool AzulGame::CanContinueRound(PlayerTable& firstPlayer, PlayerTable& secondPlayer, std::vector<Factory>& factories, TableCenter& tableCenter, Bag& bag,Logger& logger)
{
	uint16_t nrEmptyFactories = 0;

	for (Factory index : factories)
	{
		if (index.IsEmpty())
		{
			nrEmptyFactories++;
		}
	}

	if (tableCenter.IsEmpty())
	{
		nrEmptyFactories++;
	}

	if (nrEmptyFactories == factories.size() + 1)
	{
		EndOfRound(firstPlayer, secondPlayer, factories, tableCenter, bag,logger);
		return false;
	}
	else
	{
		return true;
	}
}

void AzulGame::PrintCurrentGameStatus(const PlayerTable firstPlayer, const PlayerTable secondPlayer, const std::vector<Factory>& factories, const TableCenter& tableCenter)
{
	std::cout << firstPlayer << "\n\n" << std::endl;

	for (int index = 0; index < factories.size(); index++)
	{
		std::cout << "F" << index + 1 << ": " << factories.at(index) << std::endl;
	}

	std::cout << std::endl;
	std::cout << tableCenter << std::endl;
	std::cout << secondPlayer << std::endl;
}

void AzulGame::EndOfRound(PlayerTable& firstPlayer, PlayerTable& secondPlayer, std::vector<Factory>& factories, TableCenter& tableCenter, Bag& bag,Logger& logger)
{
	bag.AddPieces(firstPlayer.MoveFromPatternLineToWall(),logger);
	bag.AddPieces(secondPlayer.MoveFromPatternLineToWall(),logger);

	FactoriesReset(bag,factories);
	logger.Log("Factories reset",Logger::Level::Info);
	tableCenter.AddFirstPlayerToken();

	firstPlayer.FlushFloorLine();
	secondPlayer.FlushFloorLine();

	for (uint16_t index = 0; index < 5; index++)
	{
		firstPlayer.CalculateScoreFromWall(index, index);
		secondPlayer.CalculateScoreFromWall(index, index);
	}

	firstPlayer.ColBonus();
	secondPlayer.ColBonus();

	std::vector<Piece::Color> colors = { Piece::Color::Black, Piece::Color::Blue, Piece::Color::Red, Piece::Color::Yellow, Piece::Color::White };
	for (auto& color : colors)
	{
		firstPlayer.ColorBonus(color);
		secondPlayer.ColorBonus(color);
	}


	std::cout << std::endl;
	std::cout << "Bag: " << bag << std::endl;

	PrintCurrentGameStatus(firstPlayer, secondPlayer, factories, tableCenter);
}

void AzulGame::FactoriesReset(Bag& bag, std::vector<Factory>& factories)
{
	for (int index = 0; index < 5; index++)
	{
		factories.at(index).AddPieces(bag.GetFourPieces());
	}
}

void AzulGame::Run()
{
	//Logger
	std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);

	

	logger.Log("Game started!", Logger::Level::Info);
	//Player definition phase
	std::string playerName;
	std::cout << "First player name: ";
	std::cin >> playerName;
	PlayerTable firstPlayer(playerName);

	std::cout << "Second player name: ";
	std::cin >> playerName;
	PlayerTable secondPlayer(playerName);

	std::reference_wrapper<PlayerTable> pickingPlayer = firstPlayer;
	std::reference_wrapper<PlayerTable> waitingPlayer = secondPlayer;

	logger.Log("Player definition phase",Logger::Level::Info);

	//Factory, Bag and TableCenter making phase
	Bag bag;
	TableCenter tableCenter;
	std::vector<Factory> factories;
	for (int index = 0; index < 5; index++)//filling factories
	{
		factories.push_back(Factory());
		factories.at(index).AddPieces(bag.GetFourPieces());
	}
	tableCenter.AddFirstPlayerToken();

	logger.Log("Factories, Bag and TableCenter are made",Logger::Level::Info);

	//Printing the table and bag
	std::cout << std::endl;
	std::cout << "Bag: " << bag;
	PrintCurrentGameStatus(waitingPlayer, pickingPlayer, factories, tableCenter);

	//Game phase
	uint16_t roundCount = 1;
	uint16_t nrFactory;
	Piece selectedColor;
	bool ok=false;
	while (!bag.IsEmpty() || firstPlayer.HasCompleteRow() || secondPlayer.HasCompleteRow())
	{
		std::cout << "ROUND " << roundCount << std::endl;
		logger.Log("New Round!",Logger::Level::Info);
		while(CanContinueRound(pickingPlayer, waitingPlayer, factories, tableCenter, bag,logger))
		{
			
		
			while (!ok)
			{logger.Log("Player is picking",Logger::Level::Info);
				std::cout << "\n\n" << pickingPlayer.get().GetName() << " pick a factory number(1-" << factories.size() << ", or 0 for center): ";
				std::cin >> nrFactory;
				while(std::cin.fail())
				{
					std::cout<<"\nWrong input!";
					logger.Log("Input error",Logger::Level::Error);
					std::cin.clear(); 
					std::cin.ignore(INT_MAX, '\n'); 
					std::cout << "\n\n" << pickingPlayer.get().GetName() << " pick a factory number(1-" << factories.size() << ", or 0 for center): ";
					std::cin >> nrFactory;
				}

				if (nrFactory == 0 && !tableCenter.IsEmpty())
				{
					std::cout << "\nAnd a piece color(ex: black): ";
					std::cin >> selectedColor;

					if (tableCenter.IsFirstPlayerTokenAvailable())
					{
						std::cout << "\n" << pickingPlayer.get().GetName() << " picked the token!";
						logger.Log("Token picked up from center",Logger::Level::Info);
						pickingPlayer.get().SetFloorLine(1,logger);
						tableCenter.RemoveFirstPlayerToken();
					}

					uint16_t numberOfPiecesSelected = tableCenter.NumberOfPickedPiece(selectedColor);
					while (!pickingPlayer.get().SetPiecesOnPatternLine(std::make_pair(numberOfPiecesSelected, selectedColor), bag,logger)) {}
					tableCenter.RemovePiece(selectedColor);

					ok = true;
				}

				else
				{
					if (nrFactory - 1 >= 0 && nrFactory - 1 < factories.size())
					{
						std::cout << "\nAnd a piece color(ex: black): ";
						std::cin >> selectedColor;
						if (factories.at(nrFactory - 1).Contains(selectedColor))
						{
							ok = true;
							uint16_t numberOfPiecesSelected = factories.at(nrFactory - 1).NumberOfPickedPiece(selectedColor);
							while (!pickingPlayer.get().SetPiecesOnPatternLine(std::make_pair(numberOfPiecesSelected, selectedColor), bag,logger)) {}
							for (Piece index : factories.at(nrFactory - 1).ReturnRestOfPieces())
							{
								tableCenter.AddPiece(index);
							}
						}
						else
						{
							std::cout << "\nFactory " << nrFactory << " does not have any pieces with the selected color!";
							logger.Log("Input error",Logger::Level::Error);
						}
					}
				
				}
			}

			std::cout << std::endl;
			PrintCurrentGameStatus(pickingPlayer, waitingPlayer, factories, tableCenter);
			std::swap(pickingPlayer, waitingPlayer);
			logger.Log("Swapped the picking player with the waiting player",Logger::Level::Info);
			ok = false;
		}
		roundCount++;
		std::swap(pickingPlayer, waitingPlayer);
		logger.Log("Swapped the picking player with the waiting player",Logger::Level::Info);
	}
}