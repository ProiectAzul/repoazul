#include "PlayerTable.h"

uint16_t PlayerTable::GetScoreTrack()
{
	return m_scoreTrack;
}

std::string PlayerTable::GetName()
{
	return m_name;
}

uint16_t PlayerTable::GetFloorLine()
{
	return m_floorLine;
}

std::array<std::array<Piece, 5>, 5> PlayerTable::GetWall()
{
	return m_wall;
}

tuple_patternLines PlayerTable::GetPattenLines()
{
	return m_patternLines;
}

void PlayerTable::SetScoreTrack(const uint16_t& scoreTrack)
{
	m_scoreTrack = scoreTrack;
}

void PlayerTable::SetFloorLine(const uint16_t& minusScoreCounter, Logger& logger)
{
	if (minusScoreCounter > 0)
	{
		for (uint16_t index = 0; index < minusScoreCounter; index++)
		{
			switch (m_floorLine)
			{
			case 1:
			{
				m_scoreTrack -= 1;
				logger.Log("Score reduced by 1",Logger::Level::Info);
				break;
			}
			case 2:
			{
				m_scoreTrack -= 1;
				logger.Log("Score reduced by 1",Logger::Level::Info);
				break;
			}
			case 3:
			{
				m_scoreTrack -= 2;
				logger.Log("Score reduced by 1",Logger::Level::Info);
				break;
			}
			case 4:
			{
				m_scoreTrack -= 2;
				logger.Log("Score reduced by 1",Logger::Level::Info);
				break;
			}
			case 5:
			{
				m_scoreTrack -= 2;
				logger.Log("Score reduced by 1",Logger::Level::Info);
				break;
			}
			case 6:
			{
				m_scoreTrack -= 3;
				logger.Log("Score reduced by 1",Logger::Level::Info);
				break;
			}
			case 7:
			{
				m_scoreTrack -= 3;
				logger.Log("Score reduced by 1",Logger::Level::Info);
				break;
			}
			};
			m_floorLine++;
		}
	}
}

void PlayerTable::FlushFloorLine()
{
	m_floorLine = 1;
}

void PlayerTable::IncreaseScoreTrack(uint16_t score)
{
	m_scoreTrack += score;
}

void PlayerTable::SetPattenLines(const tuple_patternLines& patternLines)
{
	m_patternLines = patternLines;
}

bool PlayerTable::SetPiecesOnPatternLine(const std::pair<uint16_t, Piece>& pair,Bag& bag,Logger& logger) //pair contains uint16_t for number of pieces to be added and a Piece for what kind of piece to be added
{
	uint16_t position;

	
	std::cout << "\nPick a line where you want to put the piece(s): ";
	std::cin >> position;
	while(std::cin.fail())
	{
		std::cout<<"\nWrong input!";
		logger.Log("Wrong Input",Logger::Level::Error);
		std::cin.clear(); 
		std::cin.ignore(INT_MAX, '\n'); 
		std::cout << "\nPick a line where you want to put the piece(s): ";
		std::cin >> position;
	}
	if(!VerifyIfPieceIsAlreadyOnWall(position,pair.second,logger))
		return false;

	switch (position)
	{
	case 1:
	{
		Piece tempPiece = std::get<0>(m_patternLines)[0];

		if (tempPiece == pair.second || tempPiece == Piece::Color::Null)
		{
			uint16_t temp = pair.first;

			for (auto index = std::begin(std::get<0>(m_patternLines)); index < std::end(std::get<0>(m_patternLines)); index++)
			{
				if (!(pair.first <= position))
				{
					*index = pair.second;
					SetFloorLine(static_cast<int>(pair.first) - position,logger);
				}
				if (*index == Piece::Color::Null && temp > 0)
				{
					*index = pair.second;
					temp--;
				}
			}
			std::vector<Piece> tempVector;
			for(uint16_t i=0;i<temp;i++)
				tempVector.push_back(pair.second);
			bag.AddPieces(tempVector,logger);
			SetFloorLine(temp,logger);
			return true;
		}
		else
		{
			std::cout << std::endl;
			std::cout << "Can't put different colors in the same line! " << tempPiece << " " << pair.second << std::endl;
			logger.Log("Can't put different colors in the same line!",Logger::Level::Warning);
			return false;
		}
	}
	case 2:
	{
		Piece tempPiece = std::get<1>(m_patternLines)[0];

		if (tempPiece == pair.second || tempPiece == Piece::Color::Null)
		{
			uint16_t temp = pair.first;

			for (auto index = std::begin(std::get<1>(m_patternLines)); index < std::end(std::get<1>(m_patternLines)); index++)
			{
				if (!(pair.first <= position))
				{
					*index = pair.second;
					SetFloorLine(static_cast<int>(pair.first) - position,logger);
				}
				if (*index == Piece::Color::Null && temp > 0)
				{
					*index = pair.second;
					temp--;
				}
			}
			std::vector<Piece> tempVector;
			for(uint16_t i=0;i<temp;i++)
				tempVector.push_back(pair.second);
			bag.AddPieces(tempVector,logger);
			SetFloorLine(temp,logger);
			return true;
		}
		else
		{
			std::cout << "Can't put different colors in the same line! " << tempPiece << " " << pair.second << std::endl;
			logger.Log("Can't put different colors in the same line!",Logger::Level::Warning);

			return false;
		}
	}
	case 3:
	{
		Piece tempPiece = std::get<2>(m_patternLines)[0];

		if (tempPiece == pair.second || tempPiece == Piece::Color::Null)
		{
			uint16_t temp = pair.first;
			for (auto index = std::begin(std::get<2>(m_patternLines)); index < std::end(std::get<2>(m_patternLines)); index++)
			{
				if (!(pair.first <= position))
				{
					*index = pair.second;
					SetFloorLine(static_cast<int>(pair.first) - position,logger);
				}
				if (*index == Piece::Color::Null && temp > 0)
				{
					*index = pair.second;
					temp--;
				}
			}
			std::vector<Piece> tempVector;
			for(uint16_t i=0;i<temp;i++)
				tempVector.push_back(pair.second);
			bag.AddPieces(tempVector,logger);
			SetFloorLine(temp,logger);
			return true;
		}
		else
		{
			std::cout << "Can't put different colors in the same line! " << tempPiece << " " << pair.second << std::endl;
			logger.Log("Can't put different colors in the same line!",Logger::Level::Warning);
			return false;
		}
	}
	case 4:
	{
		Piece tempPiece = std::get<3>(m_patternLines)[0];
		if (tempPiece == pair.second || tempPiece == Piece::Color::Null)
		{
			uint16_t temp = pair.first;
			for (auto index = std::begin(std::get<3>(m_patternLines)); index < std::end(std::get<3>(m_patternLines)); index++)
			{
				if (!(pair.first <= position))
				{
					*index = pair.second;
					SetFloorLine(static_cast<int>(pair.first) - position,logger);
				}
				if (*index == Piece::Color::Null && temp > 0)
				{
					*index = pair.second;
					temp--;
				}
			}
			std::vector<Piece> tempVector;
			for(uint16_t i=0;i<temp;i++)
				tempVector.push_back(pair.second);
			bag.AddPieces(tempVector,logger);
			SetFloorLine(temp,logger);
			return true;
		}
		else
		{
			std::cout << "Can't put different colors in the same line! " << tempPiece << " " << pair.second << std::endl;
			logger.Log("Can't put different colors in the same line!",Logger::Level::Warning);
			return false;
		}
	}
	case 5:
	{
		Piece tempPiece = std::get<4>(m_patternLines)[0];
		if (tempPiece == pair.second || tempPiece == Piece::Color::Null)
		{
			uint16_t temp = pair.first;
			for (auto index = std::begin(std::get<4>(m_patternLines)); index < std::end(std::get<4>(m_patternLines)); index++)
			{
				if (!(pair.first <= position))
				{
					*index = pair.second;
					SetFloorLine(static_cast<int>(pair.first) - position,logger);
				}
				if (*index == Piece::Color::Null && temp > 0)
				{
					*index = pair.second;
					temp--;
				}
			}
			std::vector<Piece> tempVector;
			for(uint16_t i=0;i<temp;i++)
				tempVector.push_back(pair.second);
			bag.AddPieces(tempVector,logger);
			SetFloorLine(temp,logger);
			return true;
		}
		else
		{
			std::cout << "Can't put different colors in the same line! " << tempPiece << " " << pair.second << std::endl;
			logger.Log("Can't put different colors in the same line!",Logger::Level::Warning);
			return false;
		}
	}
	}
	return false;

}

bool PlayerTable::VerifyIfPieceIsAlreadyOnWall(const uint16_t& positon, const Piece& piece,Logger& logger)
{
	switch(positon-1)
	{
		case 0:
		{
			for(uint16_t i=0;i<4;i++)
				if(m_wall[0][i]==piece)
				{
					std::cout<<"\nPiece is already on wall! Please pick another line: ";
					logger.Log("Piece is already on wall!!",Logger::Level::Warning);
					return false;
				}
			return true;

		}
		case 1:
		{
			for(uint16_t i=0;i<4;i++)
				if(m_wall[1][i]==piece)
				{
					std::cout<<"\nPiece is already on wall! Please pick another line: ";
					logger.Log("Piece is already on wall!!",Logger::Level::Warning);
					return false;
				}
			return true;
		}
		case 2:
		{
			for(uint16_t i=0;i<4;i++)
				if(m_wall[2][i]==piece)
				{
					std::cout<<"\nPiece is already on wall! Please pick another line: ";
					logger.Log("Piece is already on wall!!",Logger::Level::Warning);
					return false;
				}
			return true;
		}
		case 3:
		{
			for(uint16_t i=0;i<4;i++)
				if(m_wall[3][i]==piece)
				{
					std::cout<<"\nPiece is already on wall! Please pick another line: ";
					logger.Log("Piece is already on wall!!",Logger::Level::Warning);
					return false;
				}
			return true;

		}
		case 4:
		{
			for(uint16_t i=0;i<4;i++)
				if(m_wall[4][i]==piece)
				{
					std::cout<<"\nPiece is already on wall! Please pick another line: ";
					logger.Log("Piece is already on wall!!",Logger::Level::Warning);
					return false;
				}
			return true;

		}

	
	
	
	
	
	
	
	
	}
}

void PlayerTable::CalculateScoreFromWall(uint16_t x, uint16_t y)
{
	CalculateRowScore(x);
	CalculateColumnScore(y);
}

void PlayerTable::CalculateRowScore(uint16_t x)
{
	uint16_t pieceCounter = 1;
	for (uint16_t y = 0; y < 4; y++)
	{
		if (m_wall[x][y] != Piece::Color::Null && m_wall[x][y+1] != Piece::Color::Null)
			pieceCounter++;
		else
		{
			if(pieceCounter != 1)
				IncreaseScoreTrack(pieceCounter);
			pieceCounter = 1;
		}
	}
}

void PlayerTable::CalculateColumnScore(uint16_t y)
{
	uint16_t pieceCounter = 1;
	for (uint16_t x = 0; x < 4; x++)
	{
		if (m_wall[x][y] != Piece::Color::Null && m_wall[x+1][y] != Piece::Color::Null)
			pieceCounter++;
		else
		{
			if (pieceCounter != 1)
				IncreaseScoreTrack(pieceCounter);
			pieceCounter = 1;
		}
	}
}

std::vector<Piece> PlayerTable::MoveFromPatternLineToWall()
{
	std::vector<Piece> pieces;

	Piece temp = std::get<0>(m_patternLines)[0];
	std::get<0>(m_patternLines).fill(Piece::Color::Null);

	if (temp != Piece::Color::Null)
	{
		if (temp == Piece::Color::Black)
			m_wall[0][3] = temp;
		else
			if (temp == Piece::Color::Blue)
				m_wall[0][0] = temp;
			else
				if (temp == Piece::Color::Yellow)
					m_wall[0][1] = temp;
				else
					if (temp == Piece::Color::Red)
						m_wall[0][2] = temp;
					else
						if (temp == Piece::Color::White)
							m_wall[0][4] = temp;
		m_scoreTrack+=1;
	}

	temp = std::get<1>(m_patternLines)[1];
	if (temp != Piece::Color::Null)
	{
		if (temp == Piece::Color::Black)
			m_wall[1][4] = temp;
		else
			if (temp == Piece::Color::Blue)
				m_wall[1][1] = temp;
			else
				if (temp == Piece::Color::Yellow)
					m_wall[1][2] = temp;
				else
					if (temp == Piece::Color::Red)
						m_wall[1][3] = temp;
					else
						if (temp == Piece::Color::White)
							m_wall[1][0] = temp;
		pieces.insert(pieces.end(), 1, temp);
		std::get<1>(m_patternLines).fill(Piece::Color::Null);
		m_scoreTrack+=1;
	}
	else
	{
		for(auto it=std::get<1>(m_patternLines).begin();it!=std::get<1>(m_patternLines).end();it++)
			if(*it!=Piece::Color::Null)
				pieces.push_back(*it);
		std::get<1>(m_patternLines).fill(Piece::Color::Null);
	}

	temp = std::get<2>(m_patternLines)[2];
	if (temp != Piece::Color::Null)
	{
		if (temp == Piece::Color::Black)
			m_wall[2][0] = temp;
		else
			if (temp == Piece::Color::Blue)
				m_wall[2][2] = temp;
			else
				if (temp == Piece::Color::Yellow)
					m_wall[2][3] = temp;
				else
					if (temp == Piece::Color::Red)
						m_wall[2][4] = temp;
					else
						if (temp == Piece::Color::White)
							m_wall[2][1] = temp;
		pieces.insert(pieces.end(), 2, temp);
		std::get<2>(m_patternLines).fill(Piece::Color::Null);
		m_scoreTrack+=1;
	}
		else
	{
		for(auto it=std::get<2>(m_patternLines).begin();it!=std::get<2>(m_patternLines).end();it++)
			if(*it!=Piece::Color::Null)
				pieces.push_back(*it);
		std::get<2>(m_patternLines).fill(Piece::Color::Null);
	}

	temp = std::get<3>(m_patternLines)[3];
	if (temp != Piece::Color::Null)
	{
		if (temp == Piece::Color::Black)
			m_wall[3][1] = temp;
		else
			if (temp == Piece::Color::Blue)
				m_wall[3][3] = temp;
			else
				if (temp == Piece::Color::Yellow)
					m_wall[3][4] = temp;
				else
					if (temp == Piece::Color::Red)
						m_wall[3][0] = temp;
					else
						if (temp == Piece::Color::White)
							m_wall[3][2] = temp;
		pieces.insert(pieces.end(), 3, temp);
		std::get<3>(m_patternLines).fill(Piece::Color::Null);
		m_scoreTrack+=1;
	}
		else
	{
		for(auto it=std::get<3>(m_patternLines).begin();it!=std::get<3>(m_patternLines).end();it++)
			if(*it!=Piece::Color::Null)
				pieces.push_back(*it);
		std::get<3>(m_patternLines).fill(Piece::Color::Null);
	}

	temp = std::get<4>(m_patternLines)[4];
	if (temp != Piece::Color::Null)
	{
		if (temp == Piece::Color::Black)
			m_wall[4][2] = temp;
		else
			if (temp == Piece::Color::Blue)
				m_wall[4][4] = temp;
			else
				if (temp == Piece::Color::Yellow)
					m_wall[4][0] = temp;
				else
					if (temp == Piece::Color::Red)
						m_wall[4][1] = temp;
					else
						if (temp == Piece::Color::White)
							m_wall[4][3] = temp;
		pieces.insert(pieces.end(), 4, temp);
		std::get<4>(m_patternLines).fill(Piece::Color::Null);
		m_scoreTrack+=1;
	}
		else
	{
		for(auto it=std::get<4>(m_patternLines).begin();it!=std::get<4>(m_patternLines).end();it++)
			if(*it!=Piece::Color::Null)
				pieces.push_back(*it);
		std::get<4>(m_patternLines).fill(Piece::Color::Null);
	}
	return pieces;

}

PlayerTable::PlayerTable(std::string name) :m_name(name)
{
	m_scoreTrack = 0;
	m_floorLine = 1;

	for (int i = 0; i < 5; i++)
		for (int j = 0; j < 5; j++)
			m_wall[i][j] = Piece::Color::Null;

	std::get<0>(m_patternLines).fill(Piece::Color::Null);
	std::get<1>(m_patternLines).fill(Piece::Color::Null);
	std::get<2>(m_patternLines).fill(Piece::Color::Null);
	std::get<3>(m_patternLines).fill(Piece::Color::Null);
	std::get<4>(m_patternLines).fill(Piece::Color::Null);
}

PlayerTable::PlayerTable(const PlayerTable& playerTable)
{
	m_name = playerTable.m_name;
	m_scoreTrack = playerTable.m_scoreTrack;
	m_floorLine = playerTable.m_floorLine;
	m_patternLines = playerTable.m_patternLines;
	m_wall = playerTable.m_wall;
}

PlayerTable::PlayerTable(PlayerTable&& playerTable)
{
	*this = std::move(playerTable);
}

PlayerTable& PlayerTable::operator=(const PlayerTable& playerTable)
{
	m_name = playerTable.m_name;
	m_scoreTrack = playerTable.m_scoreTrack;
	m_floorLine = playerTable.m_floorLine;
	m_patternLines = playerTable.m_patternLines;
	m_wall = playerTable.m_wall;

	return *this;
}

PlayerTable& PlayerTable::operator=(PlayerTable&& playerTable)
{
	if (this != &playerTable)
	{
		m_name = playerTable.m_name;
		m_scoreTrack = playerTable.m_scoreTrack;
		m_floorLine = playerTable.m_floorLine;
		m_patternLines = playerTable.m_patternLines;
		m_wall = playerTable.m_wall;
	}

	new(&playerTable) PlayerTable;
	return *this;
}

bool PlayerTable::HasCompleteRow()
{
	bool complete;

	for (int row = 0; row < 5; ++row)
	{
		complete = true;
		for (int col = 0; col < 5; ++col)
		{
			if (m_wall[row][col] != Piece::Color::OnWall)
			{
				// This row is not complete, check the next one.
				complete = false;
				break;
			}
		}
	}

	return false;
}

void PlayerTable::RowBonus()
{
	bool complete;

	for (int row = 0; row < 5; ++row)
	{
		complete = true;
		for (int col = 0; col < 5; ++col)
		{
			if (m_wall[row][col] != Piece::Color::OnWall)
			{
				// This row is not complete, check the next one.
				complete = false;
				break;
			}
		}
		if (complete)
			IncreaseScoreTrack(2);
	}
}

void PlayerTable::ColBonus()
{
	bool complete;

	for (int col = 0; col < 5; ++col)
	{
		complete = true;
		for (int row = 0; row < 5; ++row)
		{
			if (m_wall[row][col] != Piece::Color::OnWall)
			{
				// This column is not complete, check the next one.
				complete = false;
				break;
			}
		}
		if (complete)
			m_scoreTrack += 7;
	}
}

void PlayerTable::ColorBonus(const Piece& piece)
{	
	int nr=0;
	for (int row = 0; row < 5; ++row)
	{
		
		for (int column = 0; column < 5; ++column)
		{
			if (m_wall[row][column] == piece)
			{
				nr++;
			}
			
		}
		if (nr==5)
			m_scoreTrack += 10;
	}
}

PlayerTable::~PlayerTable() {}

std::ostream& operator<<(std::ostream& os, const PlayerTable& playerTable)
{
	os << "\n" << std::endl;
	os  << "| Player: " << playerTable.m_name << std::endl;
	os << "+----------\n|Score: " << playerTable.m_scoreTrack << std::endl;
	os << "+--------------------------------------------------" << std::endl;
	
	os << "|		 ";
	std::for_each(std::begin(std::get<0>(playerTable.m_patternLines)), std::end(std::get<0>(playerTable.m_patternLines)), [](Piece i) {std::cout << i << "|"; });
	os << "->|" << playerTable.m_wall[0][0] << playerTable.m_wall[0][1] << playerTable.m_wall[0][2] << playerTable.m_wall[0][3] << playerTable.m_wall[0][4];
	os << std::endl;
	
	os << "|            ";
	std::for_each(std::begin(std::get<1>(playerTable.m_patternLines)), std::end(std::get<1>(playerTable.m_patternLines)), [](Piece i) {std::cout << i << "|"; });
	os << "->|" << playerTable.m_wall[1][0] << playerTable.m_wall[1][1] << playerTable.m_wall[1][2] << playerTable.m_wall[1][3] << playerTable.m_wall[1][4];
	os << std::endl;

	os << "|        ";
	std::for_each(std::begin(std::get<2>(playerTable.m_patternLines)), std::end(std::get<2>(playerTable.m_patternLines)), [](Piece i) {std::cout << i << "|"; });
	os << "->|" << playerTable.m_wall[2][0] << playerTable.m_wall[2][1] << playerTable.m_wall[2][2] << playerTable.m_wall[2][3] << playerTable.m_wall[2][4];
	os << std::endl;
	
	os << "|    ";
	std::for_each(std::begin(std::get<3>(playerTable.m_patternLines)), std::end(std::get<3>(playerTable.m_patternLines)), [](Piece i) {std::cout << i << "|"; });
	os << "->|" << playerTable.m_wall[3][0] << playerTable.m_wall[3][1] << playerTable.m_wall[3][2] << playerTable.m_wall[3][3] << playerTable.m_wall[3][4];
	os << std::endl;

	os << "|";
	std::for_each(std::begin(std::get<4>(playerTable.m_patternLines)), std::end(std::get<4>(playerTable.m_patternLines)), [](Piece i) {std::cout << i << "|"; });
	os << "->|" << playerTable.m_wall[4][0] << playerTable.m_wall[4][1] << playerTable.m_wall[4][2] << playerTable.m_wall[4][3] << playerTable.m_wall[4][4];
	os << std::endl;

	return os;
}

