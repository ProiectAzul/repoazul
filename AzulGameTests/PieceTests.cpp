#include "pch.h"
#include "CppUnitTest.h"
#include "../Piece.h"
#include "../Bag.h"
#include "../Factory.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace AzulGameTests
{
	TEST_CLASS(GameTests)
	{
	public:
		
		
		TEST_METHOD(ConstructorPiece)
		{
			Piece piece(Piece::Color::Black);

			Assert::IsTrue(piece==Piece::Color::Black);
		}
		
		TEST_METHOD(Print)
		{
			Piece piece(Piece::Color::Blue);

			std::stringstream stream;
			stream << piece;

			Assert::AreEqual(std::string("[Bl]"), stream.str(), L"If you see this message, piece is not printed correctly");
		}
		TEST_METHOD(TestMethodBag)
		{
			Bag bag;	
			Assert::IsTrue(bag.GetBag().size()==100);
		}
		TEST_METHOD(TestMethodFactory)
		{
			Factory factory;
			std::array<Piece,4> pieceVector={Piece(Piece::Color::Black),Piece(Piece::Color::Blue),Piece(Piece::Color::Red)};
			factory.AddPieces(pieceVector);
			std::cout<<" ";
			Assert::IsFalse(factory.IsEmpty());
		}
		TEST_METHOD(TestMethodFactory2)
		{
			Factory factory;
			std::array<Piece,4> pieceVector={Piece::Color::Black};
			factory.AddPieces(pieceVector);
			Assert::IsTrue(factory.Contains(Piece::Color::Black));
		}
	};
}

