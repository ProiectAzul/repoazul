#pragma once
#include <array>
#include <cstdint>
#include "Piece.h"
#include <tuple>
#include <vector>
#include "Bag.h"

typedef std::tuple<std::array<Piece, 1>, std::array<Piece, 2>, std::array<Piece, 3>, std::array<Piece, 4>, std::array<Piece, 5>> tuple_patternLines;

class PlayerTable
{
public:
	uint16_t GetScoreTrack();
	std::string GetName();
	uint16_t GetFloorLine();
	std::array<std::array<Piece, 5>, 5> GetWall();
	tuple_patternLines GetPattenLines();
	void SetScoreTrack(const uint16_t& scoreTrack);
	void SetFloorLine(const uint16_t& minusScoreCounter,Logger& logger);
	bool SetPiecesOnPatternLine(const std::pair<uint16_t, Piece>& pair, Bag& bag,Logger& logger);
	void SetPattenLines(const tuple_patternLines& patternLines);
	void FlushFloorLine();
	void IncreaseScoreTrack(uint16_t score);
	bool VerifyIfPieceIsAlreadyOnWall(const uint16_t& positon, const Piece& piece,Logger& logger);
	void CalculateScoreFromWall(uint16_t x, uint16_t y);
	void CalculateRowScore(uint16_t x);
	void CalculateColumnScore(uint16_t y);
	PlayerTable(std::string name = "Player");//Constructor with name for the player
	PlayerTable(const PlayerTable& playerTable); //Copy constructor, what is the "default constructor for array of array is deleted"
	PlayerTable(PlayerTable&& playerTable); //move constructor
	PlayerTable& operator=(const PlayerTable& playerTable); //= operator
	PlayerTable& operator=(PlayerTable&& playerTable);//move = operator
	friend std::ostream& operator << (std::ostream& os, const PlayerTable& playerTable);

	bool HasCompleteRow();
	void RowBonus();
	void ColBonus();
	void ColorBonus(const Piece& piece);
	std::vector<Piece> MoveFromPatternLineToWall();
	~PlayerTable();
private:
	int16_t m_scoreTrack; //uint for score track
	uint16_t m_floorLine; // uint for -score that needs to be applied to scoreTrack at the end of the round
	std::array<std::array<Piece, 5>, 5> m_wall; //the wall where player need to put pieces at the end of the round
	tuple_patternLines m_patternLines; //tuple of all arrays from the Patter Lines wall
	std::string m_name;
};

