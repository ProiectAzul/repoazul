#pragma once
#include <iostream>
#include <Windows.h>
#include <string>
#include "Logging/Logging.h"

class Piece
{
public:
	enum class Color : uint8_t
	{
		Null = 0,
		Black = 1,
		Blue = 2,
		Red = 3,
		Yellow = 4,
		White = 5,
		OnWall = 6
	};

private:
	Color m_color : 5;

public:
	Piece();
	Piece(Color color);
	Piece(const Piece& piece);
	Piece(Piece&& piece);
	~Piece();

	Piece& operator = (const Piece& piece);
	Piece& operator = (Piece&& piece);
	Piece& operator &= (const Piece& piece);
	Piece operator& (Piece piece) const;

	friend std::istream& operator >>(std::istream& is, Piece& piece);
	bool operator ==(const Piece& piece);
	bool operator !=(const Piece& piece);
	Color GetColor() const;
	void SetColor(const Color& color);
	friend std::ostream& operator << (std::ostream& os, const Piece& piece);
};