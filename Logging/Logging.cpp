#include "Logging.h"

const char* LogLevelToString(Logger::Level level)
{
	switch (level)
	{
	case Logger::Level::Info:
		return "Info";
	case Logger::Level::Warning:
		return "Warning";
	case Logger::Level::Error:
		return "Error";
	default:
		return "";
	}
}

Logger::Logger(std::ostream & os, Logger::Level minimumLevel) :
	os{ os },
	minimumLevel{ minimumLevel }
{
	// Empty
}



void Logger::Log(const std::string & message, Level level)
{
	
	if (static_cast<int>(level) < static_cast<int>(minimumLevel))
		return;

	os << "[" << LogLevelToString(level) << "] " << message << std::endl;
}

void Logger::SetMinimumLogLevel(const Level& level)
{
	this->minimumLevel = level;
}
