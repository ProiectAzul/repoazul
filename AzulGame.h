#pragma once
#include "Bag.h"
#include "Factory.h"
#include "Piece.h"
#include "PlayerTable.h"
#include "TableCenter.h"
#include <fstream>

class AzulGame {
private:

public:
	AzulGame();
	~AzulGame();
	bool CanContinueRound(PlayerTable& firstPlayer, PlayerTable& secondPlayer, std::vector<Factory>& factories, TableCenter& tableCenter, Bag& bag,Logger& logger);
	void PrintCurrentGameStatus(const PlayerTable firstPlayer, const PlayerTable secondPlayer, const std::vector<Factory>& factories, const TableCenter& tableCenter);
	void EndOfRound(PlayerTable& firstPlayer, PlayerTable& secondPlayer, std::vector<Factory>& factories, TableCenter& tableCenter, Bag& bag,Logger& logger);
	void FactoriesReset(Bag& bag, std::vector<Factory>& factories);
	void Run();
};