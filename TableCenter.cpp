#include "TableCenter.h"

TableCenter::TableCenter()
{
	m_tableCenterPieces = {};
}

TableCenter::~TableCenter() {}

void TableCenter::AddPiece(Piece piece)
{
	m_tableCenterPieces.push_back(piece);
}

void TableCenter::RemovePiece(Piece piece)
{
m_tableCenterPieces.erase(std::remove(m_tableCenterPieces.begin(), m_tableCenterPieces.end(), piece), m_tableCenterPieces.end());
}

void TableCenter::ClearTable(Piece piece)
{
	m_tableCenterPieces.clear();
}

bool TableCenter::IsEmpty()
{
	return m_tableCenterPieces.size() == 0;
}

std::vector<Piece> TableCenter::GetPieces()
{
	return m_tableCenterPieces;
}

uint16_t TableCenter::NumberOfPickedPiece(const Piece& piece)
{
	uint16_t nrPieces = 0;

	for(Piece i:m_tableCenterPieces)
		if(i==piece)
			nrPieces++;

	return nrPieces;
}

bool TableCenter::IsFirstPlayerTokenAvailable()
{
	return m_firstPlayerToken;
}

void TableCenter::RemoveFirstPlayerToken()
{
	m_firstPlayerToken = false;
}

void TableCenter::AddFirstPlayerToken()
{
	m_firstPlayerToken = true;
}



TableCenter::TableCenter(const TableCenter& tableCenter)
{
	m_tableCenterPieces = tableCenter.m_tableCenterPieces;
	m_firstPlayerToken = tableCenter.m_firstPlayerToken;
}

TableCenter::TableCenter(TableCenter&& tableCenter)
{
	*this = std::move(tableCenter);
}

TableCenter& TableCenter::operator=(const TableCenter& tableCenter)
{
	m_firstPlayerToken = tableCenter.m_firstPlayerToken;
	m_tableCenterPieces = tableCenter.m_tableCenterPieces;

	return *this;
}

TableCenter& TableCenter::operator=(TableCenter&& tableCenter)
{
	if (this != &tableCenter)
	{
		*this = std::move(tableCenter);
	}
	return *this;
}

std::ostream& operator<<(std::ostream& os, const TableCenter& tableCenter)
{
	if (tableCenter.m_firstPlayerToken == true)
	{
		os << std::endl;
		os << "First pick token ON table!" << std::endl;
	}
	else
	{
		os << std::endl;
		os << "First pick token NOT ON table!" << std::endl;
	}

	os << "Center Table: ";
	for (Piece index : tableCenter.m_tableCenterPieces)
	{
		if(index!=Piece::Color::Null)
		os << index;
	}
	os << std::endl;

	return os;
}
