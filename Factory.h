#pragma once

#include <vector>

#include "Bag.h"

class Factory {
private:
	std::array<Piece, 4> m_factoryPieces;

public:
	Factory();
	~Factory();

	void AddPieces(const std::array<Piece, 4>& pieces);
	uint16_t NumberOfPickedPiece(const Piece& piece);
	std::vector<Piece> ReturnRestOfPieces();
	std::array<Piece, 4> GetPieces();
	bool IsEmpty();
	bool Contains(const Piece& piece);

	Factory(const Factory& factory); //Copy constructor, what is the "default constructor for array of array is deleted"
	Factory(Factory&& factory); //move constructor
	Factory& operator=(const Factory& factory); //= operator
	Factory& operator=(Factory&& factory);//move = operator
	friend std::ostream& operator<<(std::ostream& os, const Factory& factory);
};

