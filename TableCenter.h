#pragma once
#include "Bag.h"
#include <vector>

class TableCenter
{
private:
	std::vector<Piece> m_tableCenterPieces;
	bool m_firstPlayerToken;


public:
	TableCenter();
	~TableCenter();

	void AddPiece(Piece piece); //add the given stones to center
	void RemovePiece(Piece piece); //remove given stone(s) from center
	void ClearTable(Piece piece); //removes all stones from table center
	bool IsEmpty(); //checks if there are no stones on center
	std::vector<Piece> GetPieces(); //get all stones on center
	uint16_t NumberOfPickedPiece(const Piece& piece);
	bool IsFirstPlayerTokenAvailable(); //return: True if the first player token is available, otherwise False
	void RemoveFirstPlayerToken(); //removes the first player token
	void AddFirstPlayerToken(); //adds the first player token


	TableCenter(const TableCenter& tableCenter); //copy constructor
	TableCenter(TableCenter&& tableCenter); //move constructor
	TableCenter& operator=(const TableCenter& tableCenter); //= operator
	TableCenter& operator=(TableCenter&& tableCenter);//move = operator
	friend std::ostream& operator << (std::ostream& os, const TableCenter& tableCenter);
};