#include <iostream>
#include <array>
#include "../Network/TcpSocket.h"

int main(int argc, char* argv[])
{
	// Validate the parameters
	if (argc != 2) {
		printf("usage: %s server-name\n", argv[0]);
		return 1;
	}
	TcpSocket socket;
	socket.Connect(argv[1], 27015);

	std::string message = "this is a test";
	socket.Send(message.c_str(), message.size() * sizeof(char));
	
	std::array<char, 512> buffer;
	int recieved;
	socket.Recieve(buffer.data(), buffer.size() * sizeof(buffer[0]), recieved);

	return 0;
}