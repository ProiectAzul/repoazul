#pragma once

#include <vector>
#include <array>
#include <cstdlib>
#include "Piece.h"

class Bag
{
private:
	std::vector<Piece> m_bag;
public:
	Bag();
	Bag(const Bag& bag);
	Bag(Bag&& bag);
	~Bag();

	std::vector<Piece> GetBag();
	Piece GetOnePiece();
	std::array<Piece, 4> GetFourPieces();

	friend std::ostream& operator << (std::ostream& os, const Bag& bag);
	bool IsEmpty();
	void AddPieces(const std::vector<Piece>& pieces,Logger& logger);

	Bag& operator=(const Bag& bag);
	Bag& operator=(Bag&& bag);
};

