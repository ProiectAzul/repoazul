#include "Piece.h"
#include <cassert>


Piece::Piece() : Piece(Color::Null) {}

Piece::Piece(Color color) : m_color(color)
{
	static_assert(sizeof(*this) <= 1, "This class should be 1 byte in size");
}

Piece::Piece(const Piece& piece)
{
	*this = piece;
}

Piece::Piece(Piece&& piece)
{
	
	*this = std::move(piece);
}

Piece::~Piece() {}

Piece& Piece::operator=(const Piece& piece)
{
	
	m_color = piece.m_color;
	return *this;
}

Piece& Piece::operator=(Piece&& piece)
{
	
	m_color = piece.m_color;
	new(&piece) Piece;
	return *this;
}

Piece& Piece::operator&=(const Piece& piece)
{
	if (this->m_color != piece.m_color)
		this->m_color = Color::Null;
	return *this;
}

Piece Piece::operator& (Piece piece) const
{
	piece &= *this;
	return piece;
}

std::istream& operator>>(std::istream& is, Piece& piece)
{
	std::string selectedColor;
	is >> selectedColor;

	while (selectedColor.compare("black") != 0 && selectedColor.compare("blue") != 0 && selectedColor.compare("white") != 0 && selectedColor.compare("red") != 0 && selectedColor.compare("yellow"))
	{
		std::cout << "\nNot valid color please insert again(ex:black): ";
		is >> selectedColor;
	}

	if (selectedColor.compare("black") == 0)
	{
		piece = Piece::Color::Black;
	}
	else
	{
		if (selectedColor.compare("blue") == 0)
		{
			piece = Piece::Color::Blue;
		}
		else
		{
			if (selectedColor.compare("white") == 0)
			{
				piece = Piece::Color::White;
			}
			else
			{
				if (selectedColor.compare("red") == 0)
				{
					piece = Piece::Color::Red;
				}
				else
				{
					if (selectedColor.compare("yellow") == 0)
					{
						piece = Piece::Color::Yellow;
					}
					else
					{
						std::cout << "The selected color is invalid!" << std::endl;
					}
				}
			}
		}
	}

	return is;
}

bool Piece::operator==(const Piece& piece)
{
	return this->GetColor() == piece.GetColor();
}

bool Piece::operator!=(const Piece& piece)
{
	return this->GetColor() != piece.GetColor();
}

Piece::Color Piece::GetColor() const
{
	return m_color;
}

void Piece::SetColor(const Color& color)
{
	m_color = color;
}

std::ostream& operator<<(std::ostream& os, const Piece& piece)
{

	Piece::Color color = piece.GetColor();
	if (color == Piece::Color::Black)
	{
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 8);
		os << "[Bl]";
	}
	else
		if (color == Piece::Color::Blue)
		{
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 9);
			os << "[Bl]";
		}
		else
			if (color == Piece::Color::White)
			{
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7);
				os << "[Wh]";
			}

			else
				if (color == Piece::Color::Red)
				{
					SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
					os << "[Re]";
				}

				else
					if (color == Piece::Color::Yellow)
					{
						SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
						os << "[Yl]";
					}

					else
						if (color == Piece::Color::Null)
						{
							SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
							os << "[*]";
						}

						else
							if (color == Piece::Color::OnWall)
							{
								//SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7);
								os << "[OW]";
							}

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
	return os;
}