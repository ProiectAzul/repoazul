#pragma once
#include <basetsd.h>
#include <cstdint>
#include <string>

class TcpSocket
{
public:
	using SocketHandle = UINT_PTR;
public:
	TcpSocket();
	~TcpSocket();
	bool Connect(const std::string& address, uint16_t port);
	bool Send(const void* data, int size);
	bool Recieve(void* data, int size, int& recieved);

	bool Listen(uint16_t port);
	TcpSocket Accept();
private:
	TcpSocket(SocketHandle socket);
private:
	SocketHandle m_socket;
};

