#include "TcpSocket.h"
#include <iostream>
#include <string>
#include <winsock2.h>
#include <ws2tcpip.h>

#pragma comment(lib, "Ws2_32.lib")

struct WinSocketInitialization
{
	WinSocketInitialization()
	{
		WSADATA wsaData;
		int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);		// initiate use of WS2_32.dll, with version 2.2
		if (iResult != 0)
		{
			throw std::string("WSAStartup failed") + std::to_string(iResult);
		}
	}

	~WinSocketInitialization()
	{
		WSACleanup();
	}
};

WinSocketInitialization globalWinSocketInitialization;

TcpSocket::TcpSocket() : m_socket(INVALID_SOCKET)
{
	// Create a SOCKET for connecting to server: IPv4, stream socket, TCP
	m_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (m_socket == INVALID_SOCKET)
	{
		throw std::string("Error at socket(): ") + std::to_string(WSAGetLastError());
	}
}

TcpSocket::~TcpSocket()
{
	if(m_socket != INVALID_SOCKET)
		closesocket(m_socket);
}

bool TcpSocket::Connect(const std::string& address, uint16_t port)
{
	// *** Connect to server ***
	addrinfo* result = NULL, hints;

	ZeroMemory(&hints, sizeof(hints));	// memset to 0
	hints.ai_family = AF_INET;			// IPv4

	// *** Resolve the server address and port (can be also names like "localhost") ***
	int iResult = getaddrinfo(address.c_str(), std::to_string(port).c_str(), &hints, &result);
	if (iResult != 0)
	{
		std::cerr << "getaddrinfo failed: " << iResult << std::endl;
		freeaddrinfo(result);
		WSACleanup();	// note: use WSACleanup when done working with sockets
		return false;
	}

	// Attempt to connect to the first address returned by the call to getaddrinfo
	iResult = connect(m_socket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR)
	{
		std::cerr << "Unable to connect to server!\n";
		closesocket(m_socket);
		m_socket = INVALID_SOCKET;
		return false;
	}
	freeaddrinfo(result);
	return true;
}

bool TcpSocket::Send(const void* data, int size)
{
	for (int sent = 0, iResult; sent < size; sent += iResult)
	{
		iResult = send(m_socket, static_cast<const char*>(data) + sent, size - sent, 0);
		if (iResult < 0)
		{
			std::cerr << "send failed: " << WSAGetLastError() << std::endl;
			return false;
		}
	}

	return true;
}

bool TcpSocket::Recieve(void* data, int size, int& recieved)
{
	recieved = recv(m_socket, static_cast<char*>(data), size, 0);
	if (recieved < 0)
	{
		std::cerr << "send failed: " << WSAGetLastError() << std::endl;
		return false;
	}
	
	return true;
}

bool TcpSocket::Listen(uint16_t port)
{
	struct addrinfo* result = NULL, hints;

	ZeroMemory(&hints, sizeof(hints));	// memset to 0
	hints.ai_family = AF_INET;			// IPv4

	// Resolve the local address and port to be used by the server
	int iResult = getaddrinfo(NULL, std::to_string(port).c_str(), &hints, &result);
	if (iResult != 0)
	{
		std::cerr << "getaddrinfo failed: " << iResult <<std::endl;
		return false;
	}

	// *** Binding the socket ***
	// Setup the TCP listening socket
	iResult = bind(m_socket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR)
	{
		std::cerr << "bind failed with error: " << WSAGetLastError() << std::endl;
		freeaddrinfo(result);
		closesocket(m_socket);
		m_socket = INVALID_SOCKET;
		return false;
	}

	// 'result' not needed anymore so free it
	freeaddrinfo(result);

	// *** Listening on the socket ***
	if (listen(m_socket, SOMAXCONN) == SOCKET_ERROR)
	{
		std::cerr << "Listen failed with error: " << WSAGetLastError() << std::endl;
		closesocket(m_socket);
		m_socket = INVALID_SOCKET;
		return false;
	}

	return true;
}

TcpSocket TcpSocket::Accept()
{
	SocketHandle socket = accept(m_socket, NULL, NULL);
	if (socket == INVALID_SOCKET)
	{
		throw std::string("accept failed: ") + std::to_string(WSAGetLastError());
	}

	return TcpSocket(socket);
}

TcpSocket::TcpSocket(SocketHandle socket) : m_socket(socket)
{
}
